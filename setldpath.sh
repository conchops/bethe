NAGLIB=/share/apps/NAG/fsl6a23dfl/lib/
ACMLIB=/share/apps/NAG/fsl6a23dfl/acml4.4.0/lib/
GCCROOT=/share/apps/gcc/4.6.2/
GCCLIB=${GCCROOT}/lib/:${GCCROOT}/lib64/

NEW_LD_PATH=${GCCLIB}:${NAGLIB}:${ACMLIB}:${LD_LIBRARY_PATH}

NAG_LICENSE="/home/cs98/.license.kusari"

export GFORTRAN_UNBUFFERED_ALL="Y"
export NAG_KUSARI_FILE=${NAG_LICENSE}

export LD_LIBRARY_PATH=${NEW_LD_PATH}