HOSTNAME := $(shell hostname)

FC=gfortran
LD=gfortran


DEBUG=-fbacktrace -ggdb -fbounds-check 
#DEBUG=-O3

WARN=-Wall -Wno-unused-dummy-argument

LDFLAGS=$(DEBUG) $(NAGDIR) $(LAPACKDIR) 
FFLAGS=$(NAGINC) $(DEBUG) $(WARN) 
LOADLIBES=$(NAGLIB)

include Makefile.$(HOSTNAME)

PROGRAMS=main
TESTS=

default: $(PROGRAMS)
	@echo "DONE Main programmes"
tests: $(TESTS)
	@echo "DONE test codes"

all: default tests
	@echo "DONE all"


TEMPLATE=
TEMPLATE: $(TEMPLATE)
	$(LD) $(LDFLAGS) $(TEMPLATE) -o $@ $(LOADLIBES) 

COMMON=Utility.o BAeqns.o Main.o
LS=

main=$(COMMON)
main: $(main)
	$(LD) $(LDFLAGS) $(main) -o $@ $(LOADLIBES) 

%.o: %.f90
	$(FC) $(FFLAGS) -c $*.f90

%.mod: %.o


%.f90:
	ln -s ~/common-code/$@ ./

clean:
	rm -f *.o *.mod $(PROGRAMS) $(TESTS)

realclean: clean
	rm -f *~ \#*
