!====
!=
!= PROGRAM: BETHE
!=
!= DESCRIPTION: Calculates, given (N,r), all Bethe
!= Ansatz energy eigenstates of the XY Model. For a given
!= (N,r) there are N!/(N-r)!r! such eigenstates. Outputs
!= BA quantum numbers, BA wavenumbers and eigenenergies
!= in "BA_wvfn_N=*_r=*.dat"
!=
!====

Program bethe
  Use utils
  Use nag_library, Only: nag_wp
  Implicit None
  Call main()
Contains


!==
  !--
  !-- SUBROUTINE: MAIN
  !--
!==

  Subroutine main()
    Implicit None
    !- Iterators
    Integer(Kind=8)::i,j,l,p,q
    Integer(Kind=8)::dim
    !- Set of allowed Bethe quantum numbers.
    !- (PA stands for Permutations in Ascending order)
    Integer(Kind=8),Allocatable::Tmp(:),Lambdas(:,:),Perms(:,:)
    !- Final outputs:
    !- k-numbers, Eigen-energies, and 
    Real(Kind=Kind(1.0D0)),Allocatable::k(:,:),kTot(:),E(:),WvVec(:,:)

    !- Sets N, r 
    Call load_params("all")
    dim=choose(N,r)

    !- To solve the BA equations in the pure XY
    !- model, it is sufficient to enumerate all
    !- sets {k_1,k_2,...,k_r} where k_i=2Pi*m_i/N
    !- and m_i is an integer in {0,1,...,N-1}
    !- such that m_i<m_{i+1}. 
    !-
    !- This collection of sets may be generated
    !- recusively starting with the set
    !- {1,2,..,r}.
    
    Call perm_ascend(N,r,Lambdas)
    Allocate(k(r,dim),kTot(dim),E(dim),WvVec(dim,dim), &
         & Tmp(r),Perms(fact(r),r))
    Tmp=(/ (i, i=1,r) /)
    Call permutate(Tmp, Perms)
    Do i=1,fact(r)
       Print*,Perms(i,:)
    End Do
    Do i=1,dim
       k(:,i)=2*Pi*(Lambdas(:,i)-1)/N
       kTot(i)=Sum(k(:,i))
       E(i)=Sum(-Cos(k(:,i)))
       !WvVec(:,i)=Sum(Perms(j,l),j=1,fact(r))
    End Do

  End Subroutine main

  !=
    !- SUBROUTINE: PERM_ASCEND
    !-
    !- DESCRIPTION: Let K={1,2,...,N}. Returns all r-sized 
    !- subsets of K, {m_1,m_2,...,m_r} such that m_i<m_{i+1}
    !- for all i in [1,r].
    !-
  !=

  Subroutine perm_ascend(N,r,PA)
    Implicit None
    !- r-sized increasing subsets from {1,2,...,N}
    Integer(Kind=8),Intent(In)::N,r
    Integer(Kind=8),Allocatable,Intent(Out)::PA(:,:)
    Integer(Kind=8),Allocatable::M(:)
    Integer(Kind=8)::i,j,k
    Allocate(M(0:r),PA(r,choose(N,r)))
    !- Init M={1,2,...,r}
    Do i=0,r
       M(i)=i
    End Do
    i=1
    Do
       PA(1:r,i)=M(1:r); i=i+1
       j=r
       !- Find the rightmost incrementable elmt.
       Do
          If ((j.Lt.1).Or.(M(j).Ne.N+j-r)) Exit
          j=j-1
       End Do
       If (j.gt.0) Then !- if exists incrementable elmt
          M(j)=M(j)+1   !- Increment it
          Do k=j+1,r    !- Set all subsequent to their min allowed
             M(k)=M(k-1)+1 
          End Do
       End If
       If (j.Eq.0) Exit
    End Do
  End Subroutine perm_ascend

  !=
    !-
    !- SUBROUTINE(R): PERMUTATE
    !-
    !- DESCRIPTION: Given a set of integers, E,
    !- outputs a matrix containing all Size(E)!
    !- permutations.
    !-
  !=

  Recursive Subroutine permutate(E, P) 
    Integer(Kind=8),Intent(In)::E(:)
    Integer(Kind=8),Intent(Out)::P(:,:)
    Integer(Kind=8)::N,Nfac,i,k,S(Size(P,1)/Size(E),Size(E)-1) 
    N = Size(E); Nfac = Size(P,1); 
    Do i=1,N
       If(N>1) Call permutate((/E(:i-1), E(i+1:)/), S) 
       ForAll(k=1:Nfac/N) P((i-1)*Nfac/N+k,:)=(/E(i), S(k,:)/) 
    End Do
  End Subroutine permutate

End Program bethe
