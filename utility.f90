Module utils
  Implicit None
Contains

  !=
    !- SUBROUTINE: LOAD_PARAMS 
    !-
    !- DESCRIPTION: 
    !- Define the problem to load, its parameters,
    !- including any complex numbers and strings.
    !-
    !- Optn is a string of 4 characters.
  !=

  Subroutine load_params(problem, num, str, optn)
    Implicit None
    character(len=*)::problem
    complex(kind=kind(1.0D0)),optional::num(:)
    character(len=16),optional::str
    character(len=4),optional::optn
    !- Select the problem
    Select Case(problem)
       Case("Test") 
          Print *, "Works."
    End Select
  End Subroutine load_params

End Module utils
