#!/bin/bash

BASE=`pwd`/../../

# Whether to actually run commands
ECHO=

EVALBIN=${BASE}/TraceEvals 
CRITBIN=${BASE}/CriticalMode

. ${BASE}/scripts/grid-common.sh

# FUNCTIONS TO INITIALISE THE START CONDITIONS FOR 
# FORTRAN BINARIES (CHANGE FOR MY USES).

write_control_common_2family(){
    ln -sf ../PARAMS/2family_modes.txt ./

    cat <<EOF > ${CONTROL}
$maxnxAtom, $maxnyAtom
$epsHOx, $epsHOy, $omegaR, $waistx, $waisty
$offsetx, $offsety
E
3
2, 0
1, 1
0, 2
2family_modes.txt
$E0, $Epump
$TF, $lTF, $NI
$Delta0Min, $Delta0Max, $NDelta0
EOF
}

write_control_common_1family(){
    ln -sf ../PARAMS/1family_modes.txt ./

    cat <<EOF > ${CONTROL}
$maxnxAtom, $maxnyAtom
$epsHOx, $epsHOy, $omegaR, $waistx, $waisty
$offsetx, $offsety
E
2
1, 0
0, 1
1family_modes.txt
$E0, $Epump
$TF, $lTF, $NI
$Delta0Min, $Delta0Max, $NDelta0
EOF
}

# Work out which othe above cases to do.
write_control_common() {
}

write_control_crit() {
    write_control_common

}

write_control_ev() {
    write_control_common

}

run_local() {

    CONTROL=CONTROL.CRIT.${prefix}
    write_control_crit
   
    echo
    $ECHO ${CRITBIN} < ${CONTROL}  

    CONTROL=CONTROL.EVAL.${prefix}
    write_control_ev

    echo
    $ECHO ${EVALBIN} < ${CONTROL} 

}

run_grid() {
    RESDIR=${BASE}/${prefix}_results

    BIN=$CRITBIN
    CONTROL=${RESDIR}/CONTROL.CRIT.${prefix}

    job_name="mm"; nproc=1
    GridScript=${RESDIR}/GRID_SCRIPT.CRIT
    write_grid_script
    write_control_crit
    
    qsub $GridScript
    
}

run() {
    if [ $HOSTNAME == "kelpie.st-and.ac.uk" ]; then
        run_grid
    else
        run_local
    fi
}

prefix="test"
run
