!====
!= MODULE: UTILS
!=
!= DESCRIPTION:
!= This module contains basic utilities common to all programs.
!=
!====

Module utils
  Implicit None
  !- Contants
  Real(Kind=Kind(1.0D0)),parameter::Pi=ACos(-1.0D0)
  !- Problem:
    !- XY: All eigenvectors for (N,r):
    Integer(Kind=8)::N,r
Contains


  !=
    !- SUBROUTINE: LOAD_PARAMS 
    !-
    !- DESCRIPTION: 
    !- Define the problem to load, its parameters,
    !- including any complex numbers and strings.
    !-
    !- Optn is a string of 4 characters.
  !=

  Subroutine load_params(problem, num, str, optn)
    Implicit None
    Character(len=*)::problem
    Complex(kind=kind(1.0D0)),optional::num(:)
    Character(len=16),optional::str
    Character(len=4),optional::optn
    !- Open params file
    Open(23,File="params",Status="Old",Action="Read")
    !- Select the problem
    Select Case(problem)
    Case("all") 
       Read(23,*) N,r
    End Select
    !- Close params file
    Close(23)
  End Subroutine load_params


  !=
    !- SUBROUTINE: SAFE_OPEN
    !-
    !- DESCRIPTION: Tries to open file, if it exists. If it does
    !- not, SAFE_OPEN creates the file as a new "ReadWrite" file.
    !-
  !=

  Subroutine safe_open(unit_num, filename)
    Implicit None
    Integer(Kind=8)::unit_num
    Character(Len=*)::filename
    Logical::extant
    Inquire(File=filename, Exist=extant)
    If (extant) Then
       Open(Unit=unit_num,File=filename,Status="Old",Action="ReadWrite")
    Else
       Open(Unit=unit_num,File=filename,Status="New",Action="ReadWrite")
    End If
  End Subroutine safe_open
  
  !=
    !- FUNCTION: ACOT
    !-
    !- DESCRIPTION: The inverse of the cotangent 
    !- function. Gives values in (-pi/2,pi/2].
    !-
  !=

  Function ACot(x)
    Implicit None
    Real(Kind=Kind(1.0D0))::x
    Real(Kind=Kind(1.0D0))::ACot
    ACot=ATan(1.0D0/x)
  End Function ACot
  
  Function CoTan(x)
    Implicit None
    Real(Kind=Kind(1.0D0))::x
    Real(Kind=Kind(1.0D0))::CoTan
    CoTan=1/Tan(x)
  End Function CoTan

  !=
    !- FUNCTION: FACT
    !-
    !- DESCRIPTION: Simple factorial function
    !-
  !=

  Recursive Function fact(n) Result(F)
    Implicit None
    Integer(Kind=8)::F
    Integer(Kind=8)::n
    If (n.eq.0) Then
       F=1
    Else
       F=fact(n-1)*n
    End If
  End Function fact

  !=
    !- FUNCTION: CHOOSE
    !-
    !- DESCRIPTION: Returns ^N C_r.
    !-
  !=

  Function choose(n,k)
    Integer(kind=8)::choose
    Integer(Kind=8)::n,k
    choose=fact(n)/(fact(n-k)*fact(k))
  End Function choose

End Module utils
