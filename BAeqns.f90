Module BAeqns
  Implicit None
Contains

  !=
    !- FUNCTION: THETA_IJ 
    !-
    !- DESCRIPTION: 
    !- Gives the phase correction to ansatz given
    !- Bethe wavenumber pair (k_i,k_j).
    !-
  !=

  Function theta_ij(ki,kj,delta)
    Implicit None
    Complex(Kind=Kind(1.0D0))::ki,kj
    Real(Kind=Kind(1.0D0))::delta
    Complex(Kind=Kind(1.0D0))::theta_ij
    theta_ij=0.0 ! NEED TO FIND TRIG FORMULA WITH DELTA.
  End Function theta_ij

End Module BAeqns
