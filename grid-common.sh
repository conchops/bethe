NAGLIB=/share/apps/NAG/fsl6a23dfl/lib/
ACMLIB=/share/apps/NAG/fsl6a23dfl/acml4.4.0/lib/
GCCROOT=/share/apps/gcc/4.6.2/
GCCLIB=${GCCROOT}/lib/:${GCCROOT}/lib64/

NEW_LD_PATH=${GCCLIB}:${NAGLIB}:${ACMLIB}:${LD_LIBRARY_PATH}

NAG_LICENSE="/home/cs98/.license.kusari"

write_grid_script() {
    # Make sure directory exists
    mkdir -p $RESDIR

    # WRite the script
    cat <<EOF > $GridScript
#!/bin/bash
#$ -S /bin/bash
#$ -wd $RESDIR
#$ -pe smp $nproc
#$ -N $job_name

export GFORTRAN_UNBUFFERED_ALL="Y"
export NAG_KUSARI_FILE=${NAG_LICENSE}
export OMP_NUM_THREADS=$nproc

export LD_LIBRARY_PATH=${NEW_LD_PATH}

$BIN < $CONTROL
EOF
}
